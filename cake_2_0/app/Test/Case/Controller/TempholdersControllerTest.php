<?php
App::uses('TempholdersController', 'Controller');

/**
 * TempholdersController Test Case
 */
class TempholdersControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.tempholder'
	);

}
