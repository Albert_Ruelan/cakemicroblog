<?php
App::uses('Tempholder', 'Model');

/**
 * Tempholder Test Case
 */
class TempholderTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.tempholder'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Tempholder = ClassRegistry::init('Tempholder');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Tempholder);

		parent::tearDown();
	}

}
