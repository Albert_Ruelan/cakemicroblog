<?php
App::uses('Hashtag', 'Model');

/**
 * Hashtag Test Case
 */
class HashtagTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.hashtag'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Hashtag = ClassRegistry::init('Hashtag');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Hashtag);

		parent::tearDown();
	}

}
