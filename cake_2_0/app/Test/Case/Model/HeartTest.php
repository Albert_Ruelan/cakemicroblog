<?php
App::uses('Heart', 'Model');

/**
 * Heart Test Case
 */
class HeartTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.heart'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Heart = ClassRegistry::init('Heart');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Heart);

		parent::tearDown();
	}

}
