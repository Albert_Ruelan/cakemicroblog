<?php
/**
 * Tweet Fixture
 */
class TweetFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'tweetsid' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'userid' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'index'),
		'content' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'tweetcreated' => array('type' => 'datetime', 'null' => false, 'default' => 'CURRENT_TIMESTAMP(6)', 'length' => 6),
		'indexes' => array(
			'PRIMARY' => array('column' => 'tweetsid', 'unique' => 1),
			'tweets_ibfk_1' => array('column' => 'userid', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'tweetsid' => 1,
			'userid' => 1,
			'content' => 'Lorem ipsum dolor sit amet',
			'tweetcreated' => '2018-08-31 08:35:33'
		),
	);

}
