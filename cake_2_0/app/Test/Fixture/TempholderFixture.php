<?php
/**
 * Tempholder Fixture
 */
class TempholderFixture extends CakeTestFixture {

/**
 * Table name
 *
 * @var string
 */
	public $table = 'tempholder';

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'tempid' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'userid' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'confirmcode' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'tempdate' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'indexes' => array(
			'PRIMARY' => array('column' => 'tempid', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'tempid' => 1,
			'userid' => 1,
			'confirmcode' => 'Lorem ipsum dolor sit amet',
			'tempdate' => 1
		),
	);

}
