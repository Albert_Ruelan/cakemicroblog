<?php
/**
 * Hashtag Fixture
 */
class HashtagFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'hashtagid' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'content' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'count' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'datecreated' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'hashtagid', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'hashtagid' => 1,
			'content' => 'Lorem ipsum dolor sit amet',
			'count' => 1,
			'datecreated' => '2018-09-10 04:37:27'
		),
	);

}
