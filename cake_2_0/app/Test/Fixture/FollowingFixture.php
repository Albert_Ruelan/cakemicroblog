<?php
/**
 * Following Fixture
 */
class FollowingFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'followid' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'userid' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'index'),
		'followersid' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'index'),
		'datefollowed' => array('type' => 'datetime', 'null' => false, 'default' => 'CURRENT_TIMESTAMP(6)', 'length' => 6),
		'indexes' => array(
			'PRIMARY' => array('column' => 'followid', 'unique' => 1),
			'userid' => array('column' => 'userid', 'unique' => 0),
			'followings_ibfk_2' => array('column' => 'followersid', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'followid' => 1,
			'userid' => 1,
			'followersid' => 1,
			'datefollowed' => '2018-08-31 03:43:35'
		),
	);

}
