<?php
/**
 * Heart Fixture
 */
class HeartFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'heartsid' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'tweetsid' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'index'),
		'userid' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'index'),
		'datehearted' => array('type' => 'datetime', 'null' => false, 'default' => 'CURRENT_TIMESTAMP(6)', 'length' => 6),
		'indexes' => array(
			'PRIMARY' => array('column' => 'heartsid', 'unique' => 1),
			'tweetsid' => array('column' => 'tweetsid', 'unique' => 0),
			'userid' => array('column' => 'userid', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'heartsid' => 1,
			'tweetsid' => 1,
			'userid' => 1,
			'datehearted' => '2018-09-07 11:26:26'
		),
	);

}
