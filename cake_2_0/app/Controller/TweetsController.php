<?php
App::uses('AppController', 'Controller');
/**
 * Tweets Controller
 *
 * @property Tweet $Tweet
 * @property PaginatorComponent $Paginator
 */
	// load model as globals so we can use the model


class TweetsController extends AppController
{
	/**
	 * Components
	 *
	 * @var array
	 */
	public $components = array('Paginator');
	public $paginate;

	//public $uses = array('User');
	/**
	 * index method
	 *
	 * @return void
	 */
	
	public function index()
	{
		//loadmodels
		$this->loadModel('User');
		$this->loadModel('Following');
		$this->loadModel('Heart');
		$this->loadModel('Hashtag');

		$this->Tweet->recursive = 0;
		//get currentid
		$currentId = $_SESSION["Auth"]["User"]["id"];
		////Current Followers and followings
		$sqlFollowing=$this->Following->getfollowing($currentId);
		$sqlFollower=$this->Following->getfollower($currentId);
		$countFollowing=$this->Following->getcountfollowersfollowing($sqlFollowing);
		$countFollower=$this->Following->getcountfollowersfollowing($sqlFollower);
		//SELECT COUNT(content),content AS NumberOfProducts FROM hashtags GROUP By content 
		$hashtags=$this->Hashtag->gethashtaglist();
		////Suggested following		
		$this->Paginator->settings["limit"] = 5;
        $this->Tweet->id = $currentId;
		if($countFollower ==0){
			$countFollower = "none";
		}
		if($countFollowing == 0){
			$countFollowing = "none";
		}
		if ($this->request->is('post')) {
			$this->add();
		}
		//$heart=$this->Heart->findByuserid($_SESSION["Auth"]["User"]["id"]);
		$heart=$this->Heart->find('all',array('heart.userid'=> $_SESSION["Auth"]["User"]["id"]));

		$suggestedpeople = $this->Following->getsuggest($sqlFollowing);
		$sqlUser=$this->User->getuser($currentId);
		$this->set('countfollowing',$countFollowing);
		$this->set('countfollower', $countFollower);
		$this->set('tweets', $this->paginate('Tweet'));
		$this->set('followers', $sqlFollower);
		$this->set('following',$sqlFollowing);
		$this->set('user', $sqlUser);
		$this->set('suggesteds', $suggestedpeople);
		$this->set('hashtags',$hashtags);
		$this->set('hearts',$heart);
	}

	public function viewfollowing($id =null){
		$this->redirect(array('controller' => 'Followings', 'action' => 'index',$id));

	}
	public function viewfollower($id= null){
		$this->redirect(array('controller' => 'Followings', 'action' => 'followers',$id));
	}
	/**
	 * hearts method
	 *  @throws NotFoundException
	 * @param string $id
	 * @return void
	 */


	/**
	 * 
	 * view method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	
	public function view($id = null)
	{
		if (!$this->Tweet->exists($id)) {
			throw new NotFoundException(__('Invalid tweet'));
		}
		$options = array('conditions' => array('Tweet.' . $this->Tweet->primaryKey => $id));
		$this->set('tweet', $this->Tweet->find('first', $options));
	}
	/**
	 * add method
	 *
	 * @return void
	 */
	public function viewhashtags($content){
		$this->redirect(array('controller' => 'Hashtags', 'action' => 'view',$content));
	}
	public function followme($id){
		$this->redirect(array('controller' => 'Followings', 'action' => 'add',$id));
	}

	public function viewUser($id = null)
	{
		
		$this->loadModel("User");
		$this->loadModel("Tweet");
		$this->loadModel("Following");
		$this->Tweet->recursive = 0;
		$this->User->id = $id;
		$this->redirect(array('controller' => 'Users', 'action' => 'view',$id));

		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}

		$this->paginate['order'] = array('Thread.created' => 'desc');
		$this->paginate['conditions'] = array('tweeter.userid' => $id) ;
		$this->Paginator->settings["limit"] = 5;

		$this->set('tweets', $this->Paginator->paginate(array('Tweet.userid' => $id)));
		$this->set('user', 	$sqlUser=$this->User->getuser($id));
		$this->set('followers',$sqlFollower=$this->Following->getfollower($id));

		$this->set('suggesteds',$this->Following->getsuggest($sqlFollower));
		$this->render('/Users/view');
	}
	public function addcomment($id=null){
		$this->redirect(array('controller' => 'Comments', 'action' => 'add',$id));
	}
	public function add()
	{
		
		if ($this->request->is('post')) {
			$this->loadModel("Hashtag");
			
			$this->Tweet->create();//$this->request->data["Tweet"]["content"]
			$this->request->data['Tweet']['userid'] = $_SESSION["Auth"]["User"]["id"];
			$this->request->data['Tweet']['tweetcreated'] = $mysqltimeCurrent = date("Y-m-d H:i:s");
			$this->request->data['Tweet']['content']=$this->test_input($this->data['Tweet']['content']);

			if ($this->Tweet->save($this->request->data)) {
				$id = $this->Tweet->getLastInsertId();
				$this->Hashtag->hashtags($id,$this->data['Tweet']['content'],$mysqltimeCurrent = date("Y-m-d H:i:s"));
				$this->Flash->success(__('The tweet has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The tweet could not be saved. Please, try again.'));
			}
		}
	}
	function test_input($data)
	{
		  $data = trim($data);
		  $data = stripslashes($data);
		  $data = htmlspecialchars($data);
		  return $data;
	}

	/**
	 * edit method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function edit($id = null)
	{
		$this->Tweet->id = $id;
		if (!$this->Tweet->exists($id)) {
			throw new NotFoundException(__('Invalid tweet'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Tweet->save($this->request->data)) {
				$this->Flash->success(__('The tweet has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The tweet could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Tweet.' . $this->Tweet->primaryKey => $id));
			$this->request->data = $this->Tweet->find('first', $options);
		}
	}

	/**
	 * delete method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function heart($id = null)
	{		
		$currentId = $_SESSION["Auth"]["User"]["id"];
		$this->loadModel("Heart");
			
		$mysqltimeCurrent = date("Y-m-d H:i:s");

		$this->Heart->data = array('tweetsid'=> $id , 'userid'=>$currentId);

		$this->Tweet->id = $id;
		if (!$this->Tweet->exists()) {
			throw new NotFoundException(__('Invalid tweet'));
		}
			if ($this->Heart->save($this->Heart->data)) {
				$this->Flash->success(__('The tweet has been hearted.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The tweet could not be saved. Please, try again.'));
			}
			$options = array('conditions' => array('Tweet.' . $this->Tweet->primaryKey => $id));
			$this->request->data = $this->Tweet->find('first', $options);
	}
	public function delete($id = null)
	{
		$this->loadModel("Hashtag");
		$this->Tweet->id = $id;
		$tweethash=$this->Hashtag->findByhashtagtweetid($id);
		$this->Hashtag->id = $tweethash["Hashtags"]["hashtagid"];	

		if (!$this->Tweet->exists()) {
			throw new NotFoundException(__('Invalid tweet'));
		}
		$this->request->allowMethod('post', 'delete');
		if (!empty($tweethash)) {
			if($this->Hashtag->deleteAll(array('Hashtag.hashtagtweetid' => $id))){
			$this->Tweet->delete();
			$this->Flash->success(__('The tweet has been deleted.'));}
		} elseif(empty($tweethash)){
			$this->Tweet->delete();
			$this->Flash->success(__('The tweet has been deleted.'));
		}else {
			$this->Flash->error(__('The tweet could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
	public function unheart($id){
		return $this->redirect(array('controller'=>'Comments','action' => 'unheart',$id));
	}
}
