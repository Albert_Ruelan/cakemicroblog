<?php
// app/Controller/AppController.php
class AppController extends Controller {
    //...
//redirect in cake $this->redirect(array('controller' => 'my_controller', 'action' => 'my_action'));
    public $components = array(
        'Flash',
        'Auth' => array(
            'loginRedirect' => array(
                'controller' => 'tweets',
                'action' => 'index'
            ),
            'logoutRedirect' => array(
                'controller' => 'users',
                'action' => 'login',
            ),
            'authenticate' => array(
                'Form' => array(
                    'passwordHasher' => 'Blowfish'
                )
            )
        )
    );
    public function beforeFilter() {
        $this->Auth->allow('index', 'view');
            parent::beforeFilter();
                $this->layout = 'template';
          
    }
    //...
}
date_default_timezone_set('Asia/Manila');
?>