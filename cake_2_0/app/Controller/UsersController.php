<?php
// app/Controller/UsersController.php
App::uses('AppController', 'Controller');

class UsersController extends AppController
{
    public $paginate;

    public $components = array('Paginator');

    public function beforeFilter()
    {
        parent::beforeFilter();
        // Allow users to register and logout.
        $this->Auth->allow('add', 'logout');
    }

    public function login()
    {

        if ($this->request->is('post')) {
            $this->loadModel("Tempholder");
            $tempchecker=$this->User->findByusername($_REQUEST["data"]["User"]["username"]);
            if(!empty($tempchecker))
            $checkEmail=$this->Tempholder->findByuserid($tempchecker["User"]["id"]);
            $string = "";;
            if(!empty($checkEmail)){
                $string .= "Verify Your account first \n";
                echo 	'<script type="text/javascript">alert("Verify your account first")</script>';
            }
            if ($this->Auth->login() && empty($checkEmail) == true) {
                return $this->redirect($this->Auth->redirectUrl());
            }  else{ 
            $this->Flash->error(__('Invalid username or password, try again'));
                    $string .= "Invalid username or password";
            echo 	'<script type="text/javascript">alert("'.$string .'")</script>';}
        }
    }
    public function viewfollowing($id =null){
		$this->redirect(array('controller' => 'Followings', 'action' => 'index',$id));

	}
	public function viewfollower($id= null){
		$this->redirect(array('controller' => 'Followings', 'action' => 'followers',$id));
	}

    public function logout()
    {
        return $this->redirect($this->Auth->logout());
    }

    public function index()
    {
        $this->User->recursive = 0;
        $this->set('users', $this->paginate());
    }


    public function view($id = null)
    {
        $this->recursive = -1;
        $this->loadModel("Tweet");
        $this->loadModel("Following");
        $tweets = $this->Tweet->profiletweets($id);

        $this->User->id = $id;

        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        $this->Tweet->id = $id;
        $this->paginate['order'] = array('Thread.created' => 'desc');
        $this->paginate['conditions'] = array('tweeter.userid' => $id);
        $this->Paginator->settings["limit"] = 5;
        $this->set('tweets', $this->Tweet->usertweetslist($id));
        $this->set('user', $sqlUser = $this->User->getuser($id));
        $this->set('followers', $sqlFollower = $this->Following->getfollower($id));
        $sqlFollowing=$this->Following->getfollowing($id);
		$sqlFollower=$this->Following->getfollower($id);
        $countFollowing=$this->Following->getcountfollowersfollowing($sqlFollowing);
        $countFollower=$this->Following->getcountfollowersfollowing($sqlFollower);
        $checkfollow=$this->Following->find('all', array('conditions' => array("Following.followersid"=> $_SESSION["Auth"]["User"]["id"],"Following.userid"=>$id)));
        if($countFollower ==0){
			$countFollower = "none";
		}
		if($countFollowing == 0){
			$countFollowing = "none";
		}
        
		$this->set('countfollowing',$countFollowing);
		$this->set('countfollower', $countFollower);
        $this->set('suggesteds', $this->Following->getsuggest($sqlFollower));
        $this->set('checkfollow',$checkfollow);
    }
    public function add()
    {
        if ($this->request->is('post')) {
            $this->User->create();
            $this->loadModel("Tempholder");


            $filename = '';
            $uploadData = $this->data['User']['picture'];
            $filename = basename($uploadData['name']);
            $uploadFolder = WWW_ROOT . 'img';
            $filename = time() . '_' . $filename;
            $uploadPath = $uploadFolder . DS . $filename;
            if($uploadData["type"] != "image/jpeg" && $uploadData["type"] != "image/png"&& $uploadData["type"] != "image/png"){
                echo 	'<script type="text/javascript">alert("File is not an image")</script>';
            }else{
            $this->request->data['User']['picture'] = $filename;
            $this->request->data['User']['username'] = $this->test_input($this->data['User']['email']);

            if (!file_exists($uploadFolder)) {
                mkdir($uploadFolder);
            }
            if (!move_uploaded_file($uploadData['tmp_name'], $uploadPath)) {
                return false;
            }
            if ($this->User->save($this->request->data)) {
              //  $this->set('image',$file_name1); 
                $confirm_code = md5(uniqid(rand()));
                $email = $this->data['User']['email'];
                $sqlFollowing = $this->User->sendConfirmation($confirm_code, $email);
                $id = $this->User->getLastInsertId();
                $this->Tempholder->addconfirmcode($confirm_code, $id);

                $this->Flash->success(__('The user has been saved'));
                return $this->redirect(array('controller'=>'Users','action' => 'login'));
            }
        }
        }
        $this->Flash->error(
            __('The user could not be saved. Please, try again.')
        );
    }
    public function test_input($data)
	{
		  $data = trim($data);
		  $data = stripslashes($data);
		  $data = htmlspecialchars($data);
		  return $data;
	}
    public function find($string)
    {
        $list = [];
        $lastname = $this->User->findBylastname($string);
        if ($lastname != null)
            array_push($list, $lastname);
        $firstname = $this->User->findByfirstname($string);
        if ($firstname != null)
            array_push($list, $firstname);
        $username = $this->User->findByusername($string);
        array_push($list, $username);
        $list = $this->User->unique_multidim_array($list, 'id');
        $this->set("listofsearch", $list);
    }
    public function edit($id = null)
    {
       // ifp[]
        if ($_SESSION["Auth"]["User"]["id"] == $id) {
            $this->User->id = $id;
            if (!$this->User->exists()) {
                throw new NotFoundException(__('Invalid user'));
            }
            if ($this->request->is('post') || $this->request->is('put')) {
                $filename = '';
                $uploadData = $this->data['User']['picture'];
                $filename = basename($uploadData['name']);
                $uploadFolder = WWW_ROOT . 'img';
                $filename = time() . '_' . $filename;
                $uploadPath = $uploadFolder . DS . $filename;
                $this->request->data['User']['picture'] = $filename;
                $this->request->data['User']['username'] = $this->data['User']['email'];

                if (!file_exists($uploadFolder)) {
                    mkdir($uploadFolder);
                }
                if (!move_uploaded_file($uploadData['tmp_name'], $uploadPath)) {
                    return false;
                }
                if ($this->User->save($this->request->data)) {
                    $this->Flash->success(__('The user has been saved'));
                    return $this->redirect(array('controller'=>'tweet','action' => 'index'));
                }
                $this->Flash->error(
                    __('The user could not be saved. Please, try again.')
                );
            } else {
                $this->request->data = $this->User->findById($id);
                unset($this->request->data['User']['password']);
            }
        }else{
            throw new NotFoundException(__('Invalid user'));

        }
    }/*
     */
    public function delete($id = null)
    {
        // Prior to 2.5 use
        // $this->request->onlyAllow('post');
        $this->request->allowMethod('post');
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->User->delete()) {
            $this->Flash->success(__('User deleted'));
            return $this->redirect(array('action' => 'index'));
        }
        $this->Flash->error(__('User was not deleted'));
        return $this->redirect(array('action' => 'index'));
    }

}
?>