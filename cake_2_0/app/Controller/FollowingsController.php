<?php
App::uses('AppController', 'Controller');
/**
 * Followings Controller
 *
 * @property Following $Following
 * @property PaginatorComponent $Paginator
 */
class FollowingsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index($id = null) {
		$this->loadmodel("User");
		$this->loadmodel("Tweet");

		if (!$this->User->exists($id)) {
		}
		else{
		$this->Following->id = $id;
		$this->Following->recursive = 0;
		$this->Paginator->settings = array(
			'joins' => array(	
				array(
				'table' => 'users',
				'alias' => 'UserJoin',
				'type' => 'INNER',
				'conditions' => array(
					'UserJoin.id = Following.userid'
				)
			)
			),
			'conditions' => array(
				'Following.followersid' => $id
			),
			'fields' => array('UserJoin.*', 'Following.*'),
			'order' => 'Following.datefollowed DESC'
			
		);
		$sqlfollower=$this->Following->getfollowing($id);
		$arrayCtweet = [];
		if(empty($sqlfollower) == false){
		foreach($sqlfollower as $follower){
			$pending = $this->Tweet->find('count',
			 array(
				'conditions' => array('Tweet.userid' => $follower["Following"]["userid"])
			));
			$arraypending = array("Userid" => $follower["UserJoin"]["username"],"NumberofTweets" => $pending);
			array_push($arrayCtweet,$arraypending);
		}}
		$arrayCtweet=$this->Following->bubblesort($arrayCtweet);
		$sql2=$this->Paginator->paginate();
		$this->set('followings',$sql2);
		$this->set('actives',$arrayCtweet);
		}
	}
	public function followers($id = null) {
		$this->loadmodel("User");
		$this->loadmodel("Tweet");

		if (!$this->User->exists($id)) {
		}
		else{
		$this->Following->id = $id;
		$this->Following->recursive = 0;
		$this->Paginator->settings = array(
			'joins' => array(	
				array(
				'table' => 'users',
				'alias' => 'UserJoin',
				'type' => 'INNER',
				'conditions' => array(
					'UserJoin.id = Following.followersid'
				)
			)
			),
			'conditions' => array(
				'Following.userid' => $id
			),
			'fields' => array('UserJoin.*', 'Following.*'),
			'order' => 'Following.datefollowed DESC'
			
		);
		$sqlfollower=$this->Following->getfollower($id);
		$arrayCtweet = [];
		if(empty($sqlfollower) == false){
		foreach($sqlfollower as $follower){
			$pending = $this->Tweet->find('count', array(
				'conditions' => array('Tweet.userid' => $follower["Following"]["userid"])
			));
			$arraypending = array("Userid" => $follower["UserJoin"]["username"],"NumberofTweets" => $pending);
			array_push($arrayCtweet,$arraypending);
		}
	}
		$arrayCtweet=$this->Following->bubblesort($arrayCtweet);
		$sql2=$this->Paginator->paginate();
		$this->set('followings',$sql2);
		$this->set('actives',$arrayCtweet);
		}
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->redirect(array('controller' => 'Users', 'action' => 'view',$id));

	}

/**
 * add method
 *
 * @return void
 */
	public function add($id) {
			$this->Following->create();
			$this->request->data['Following']['userid'] = $id;
			$this->request->data['Following']['followersid'] = $_SESSION["Auth"]["User"]["id"];
			$this->request->data['Following']['datefollowed'] = $mysqltimeCurrent = date("Y-m-d H:i:s");
			if ($this->Following->save($this->request->data)) {
			} else {
				$this->Flash->error(__('The following could not be saved. Please, try again.'));
			}
		$this->redirect(array('controller' => 'Followings', 'action' => 'index', $_SESSION["Auth"]["User"]["id"]));

	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Following->exists($id)) {
			throw new NotFoundException(__('Invalid following'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Following->save($this->request->data)) {
				$this->Flash->success(__('The following has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The following could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Following.' . $this->Following->primaryKey => $id));
			$this->request->data = $this->Following->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$sql=$this->Following->find('all',array('conditions'=>array('Following.userid'=>$id,'Following.followersid'=>$_SESSION["Auth"]["User"]["id"])));
		$this->Following->id = $sql[0]["Following"]["followid"];
		if (!$this->Following->exists()) {
			throw new NotFoundException(__('Invalid following'));
		}
		if ($this->Following->delete()) {
			$this->Flash->success(__('The following has been deleted.'));
		} else {
			$this->Flash->error(__('The following could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('controller'=>'Users','action' => 'view',$id));
	}
}
	
