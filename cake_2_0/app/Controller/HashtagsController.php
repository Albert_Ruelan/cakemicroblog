<?php
App::uses('AppController', 'Controller');
/**
 * Hashtags Controller
 *
 * @property Hashtag $Hashtag
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 * @property FlashComponent $Flash
 */
class HashtagsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Hashtag->recursive = 0;
		$this->set('hashtags', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($content = null) {
		$list=$this->Hashtag->gethash($content);
		$this->set('hashtags',$list);
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Hashtag->create();
			if ($this->Hashtag->save($this->request->data)) {
				$this->Flash->success(__('The hashtag has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The hashtag could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Hashtag->exists($id)) {
			throw new NotFoundException(__('Invalid hashtag'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Hashtag->save($this->request->data)) {
				$this->Flash->success(__('The hashtag has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The hashtag could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Hashtag.' . $this->Hashtag->primaryKey => $id));
			$this->request->data = $this->Hashtag->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Hashtag->id = $id;
		if (!$this->Hashtag->exists()) {
			throw new NotFoundException(__('Invalid hashtag'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Hashtag->delete()) {
			$this->Flash->success(__('The hashtag has been deleted.'));
		} else {
			$this->Flash->error(__('The hashtag could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
