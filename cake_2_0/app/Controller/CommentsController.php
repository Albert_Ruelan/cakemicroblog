<?php
App::uses('AppController', 'Controller');
/**
 * Comments Controller
 *
 * @property Comment $Comment
 * @property PaginatorComponent $Paginator
 */
class CommentsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
public $paginate;

	public function index() {
		$this->Comment->recursive = 0;
		$this->Paginator->settings["limit"] = 1;
		$this->paginate['limit'] = 1;
		$this->set('comments', $this->Paginator->paginate());

	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Comment->exists($id)) {
			throw new NotFoundException(__('Invalid comment'));
		}
		$options = array('conditions' => array('Comment.' . $this->Comment->primaryKey => $id));
		$this->set('comment', $this->Comment->find('first', $options));
	}
	public function heart($id = null)
	{		
		$currentId = $_SESSION["Auth"]["User"]["id"];
		$this->loadModel("Heart");
		$this->loadModel("Tweet");
			
		$mysqltimeCurrent = date("Y-m-d H:i:s");

		$this->Heart->data = array('tweetsid'=> $id , 'userid'=>$currentId);

		$this->Tweet->id = $id;
		if (!$this->Tweet->exists()) {
			throw new NotFoundException(__('Invalid tweet'));
		}
			if ($this->Heart->save($this->Heart->data)) {
				$this->Flash->success(__('The tweet has been hearted.'));
				return $this->redirect(array('action' => 'add',$id));
			} else {
				$this->Flash->error(__('The tweet could not be saved. Please, try again.'));
			}
			$options = array('conditions' => array('Tweet.' . $this->Tweet->primaryKey => $id));
			$this->request->data = $this->Tweet->find('first', $options);
	}
	public function unheart($id = null)
	{		
		$this->loadModel("Heart");
		$this->Heart->id = $id;

		$this->request->allowMethod('post', 'delete');
		if ($this->Heart->delete()) {
			$this->Flash->success(__('The comment has been deleted.'));
			return $this->redirect(array('controller'=>'Tweets','action' => 'index'));

		} else {
			$this->Heart->error(__('The comment could not be deleted. Please, try again.'));
		}
	}

/**
 * add method
 *
 * @return void
 */
	public function add($id = null) {//$_POST["data"]["Retweet"]
		$this->loadModel("Heart");
		$this->loadModel("Tweet");
		$this->Comment->id = $id;	
		$this->Tweet->id = $id;
		$this->Heart->id = $id;
		$sql=$this->Comment->getcomments($id);
		$tweet=$this->Tweet->findBytweetsid($id);
		$numberofpeople=$this->Heart->getpeopleheart($id);
		$heart=$this->Heart->find('first',array('conditions' => array('Heart.userid' => $_SESSION["Auth"]["User"]["id"],'Heart.tweetsid' => $id)));
		if ($this->request->is('post')) {
			$postvalue=array_keys($_POST["data"]);
			if($postvalue[0] == "Retweet"){
				$this->retweet($id,$this->request->data["Retweet"]["content"]);
			}
			elseif($postvalue[0] == "Comment"){
			$this->request->data["Comment"]["userid"] = $_SESSION["Auth"]["User"]["id"];
			$this->request->data["Comment"]["tweetid"] = $id;
			$this->request->data["Comment"]["datecreated"] = date("Y-m-d H:i:s");
			$this->Comment->create();
			if ($this->Comment->save($this->request->data)) {
				$this->Flash->success(__('The comment has been saved.'));
				return $this->redirect(array('action' => 'add',$id));
			} else {
				$this->Flash->error(__('The comment could not be saved. Please, try again.'));
			}
			}
		}
		$this->set('tweetdetails',$tweet);
		$this->set('comments',$sql);
		$this->set('heart',$heart);
		$this->set('numpeoplehearts',$numberofpeople);
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function retweet($id=null,$string=null){
		$this->loadModel("Tweet");
		$this->loadModel("User");
		$this->loadModel("Hashtag");
		$tweet=$this->Tweet->findBytweetsid($id);
		$user=$this->User->getuser($tweet["Tweet"]["userid"]);
		$this->Tweet->create();
		$this->request->data['Tweet']['userid'] = $_SESSION["Auth"]["User"]["id"];	
		$this->request->data['Tweet']['content'] = "Retweet From: ".$user["User"]["firstname"]." ".$user["User"]["lastname"]." \n " .$string." \n ".$tweet["Tweet"]["content"];
		$this->request->data['Tweet']["tweetcreated"] = $mysqltimeCurrent = date("Y-m-d H:i:s");
		if ($this->Tweet->save($this->request->data)) {
			$this->Flash->success(__('The tweet has been saved.'));
			$id = $this->Tweet->getLastInsertId();
			$this->Hashtag->hashtags($id,$this->data['Tweet']['content'],$mysqltimeCurrent = date("Y-m-d H:i:s"));
			return $this->redirect(array('controller'=>'Tweets','action' => 'index'));
		}

	}

	public function edit($id = null) {
		if (!$this->Comment->exists($id)) {
			throw new NotFoundException(__('Invalid comment'));
		}
		$sample=$this->Comment->findBycommentid($id);
		if ($this->request->is(array('post', 'put'))) {
			$this->Comment->id = $id;
			$this->request->data["Comment"]["userid"] = $sample["Comment"]["userid"];
			$this->request->data["Comment"]["tweetid"] = $sample["Comment"]["tweetid"];
			$this->request->data["Comment"]["content"] = $this->request->data["Comment"]["content"];
			$this->request->data["Comment"]["datecreated"] = date("Y-m-d H:i:s");

			if ($this->Comment->save($this->request->data)) {
				$this->Flash->success(__('The comment has been saved.'));
				return $this->redirect(array('action' => 'add',$sample["Comment"]["tweetid"]));
			} else {
				$this->Flash->error(__('The comment could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Comment.' . $this->Comment->primaryKey => $id));
			$this->request->data = $this->Comment->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$sample=$this->Comment->findBycommentid($id);
		$this->Comment->id = $id;
		if (!$this->Comment->exists()) {
			throw new NotFoundException(__('Invalid comment'));
		}
		if ($this->Comment->delete()) {
			$this->Flash->success(__('The comment has been deleted.'));
		} else {
			$this->Flash->error(__('The comment could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'add',$sample["Comment"]["tweetid"]));
	}
}
