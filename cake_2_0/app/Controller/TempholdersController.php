<?php
App::uses('AppController', 'Controller');
/**
 * Tempholders Controller
 *
 * @property Tempholder $Tempholder
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 * @property FlashComponent $Flash
 */
class TempholdersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');

	public function index($id = null) {
		$checker=$this->Tempholder->findconfirmcode($id);
		if($checker == null){
			$stringver = "Confirmation code not found :";
			$stringresult = "Failed.";
			$stringredirect = "Redirect in :";
		}else{
			$this->Tempholder->id = $checker["Tempholder"]["tempid"];
			$this->Flash->set('Email has been confirmed.', [
				'element' => 'success'
			]);
			$stringver = "Your email is now :";
			$stringresult = "Verified";
			$stringredirect = "Redirect in :";
		}
		$this->Tempholder->delete();

		$this->set("confirmation",$stringver);
		$this->set("result",$stringresult);
		$this->set("stringredirect",$stringredirect);

		
    }
}
