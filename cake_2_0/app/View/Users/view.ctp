<div class="gtco-section">
			<div class="row">
				<div class="col-md-8 col-md-offset-2 text-center gtco-heading animate-box">
					<h2>Profile Page</h2>
					<p>User specific Posts</p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 col-sm-6">
					<div class="feature-center animate-box" data-animate-effect="fadeIn">
						<span class="icon">
							<i>1</i>
						</span>
						<?php
							$path = 'img/'.$user["User"]["picture"].'';
							echo '<img src="/cake_2_0/app/webroot/img/'.$user["User"]["picture"].'" height="200"  ><br>'; 
							echo 'Hello : '.$user["User"]["firstname"] ." ".$user["User"]["lastname"];
							echo '<br> Username :'.$user["User"]["username"];
							echo '<br> Date Joined: :'.$user["User"]["created"];
						?>
						<h3>Total Number of followers : <?php echo $this->Html->link(__($countfollower), array('action' => 'viewfollower', $user["User"]["id"]));  ?></h3>
						<h3>Total Number of followings: <?php echo $this->Html->link(__($countfollowing), array('action' => 'viewfollowing', $user["User"]["id"])); ?></h3>
						<?php  if(!empty($checkfollow)){$buttonvalue = "Unfollow"; $adddelete ='delete';} else{$buttonvalue = "Follow"; $adddelete ='add';}?>
							<button onclick="window.location.href='<?php echo Router::url(array('controller'=>'Followings', 'action'=>$adddelete, $dataForView["user"]["User"]["id"]))?>'"><?php echo $buttonvalue;?></button>
						</div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="feature-center animate-box" data-animate-effect="fadeIn">
						<span class="icon">
							<i>2</i>
						</span>
						<table cellpadding="0" cellspacing="0">
							<thead>
							<tr>
							
									<th><?php echo $this->Paginator->sort('tweetsid'); ?></th>
									<th><?php echo $this->Paginator->sort('userid'); ?></th>
									<th><?php echo $this->Paginator->sort('content'); ?></th>
									<th><?php echo $this->Paginator->sort('tweetcreated'); ?></th>
									<th class="actions"><?php echo __('Actions'); ?></th>
							</tr>
							</thead>
							<tbody>
							<?php 
								$i=0; ?>
							<tr>
							<?php if(count($tweets) != 0):?>
							<?php while($i<count($tweets)): ?>
							
								<?php if($user["User"]["id"]==$tweets[$i]["Tweet"]["userid"]):?> 
									<td><?php echo '<img src="/cake_2_0/app/webroot/img/'.$user["User"]["picture"].'" height="200"  style="height:100px;width:100px;float:left;border-radius: 8px;" ><br>'; ?>&nbsp;</td>&nbsp;</td>
									<td><?php echo h($user["User"]["id"]); ?>&nbsp;</td>
									<td><?php echo h($tweets[$i]["Tweet"]["content"]); ?>&nbsp;</td>
									<td><?php echo h($tweets[$i]["Tweet"]["tweetcreated"]); ?>&nbsp;</td>
									<td class="actions">
										<?php if($_SESSION["Auth"]["User"]["id"]==$user["User"]["id"]): ?>
										<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete',$tweets[$i]["Tweet"]["userid"]), array('confirm' => __('Are you sure you want to delete # %s?', $tweets[$i][0]['tweetsid']))); ?>
										<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $tweets[$i]["Tweet"]["userid"])); ?>
										<?php endif; ?>
										<?php echo $this->Html->link(__('Comment'), array('controller' => 'Tweets','action' => 'addcomment', $tweets[$i]["Tweet"]["tweetsid"])); ?>
										<?php echo $this->Html->link(__('Heart'), array('controller' => 'Tweets','action' => 'heart', $_SESSION["Auth"]["User"]["id"])); ?>
									</td>
									</tr>
									<?php endif; $i++; ?>
							<?php $i++; endwhile; ?>
							<?php endif; ?>


							</tbody>
							</table>
                            <div class="paging">
                                <?php
                                    echo $this->Paginator->prev('< ' . __('previous '), array(), null, array('class' => 'prev disabled'));
                                    echo $this->Paginator->numbers(array('separator' => '  '));	
                                    echo $this->Paginator->next(__(' next') . ' >', array(), null, array('class' => 'next disabled'));
                                ?>
                            </div>

						</div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="feature-center animate-box" data-animate-effect="fadeIn">
						<span class="icon">
							<i>3</i>
						</span>

						</div>
				</div>
				

	</div>

<div class="tweets index">



</div>