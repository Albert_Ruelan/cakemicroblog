<header id="gtco-header" class="gtco-cover gtco-cover-md" role="banner" style="background-image: url(images/img_bg_2.jpg)">
		<div class="overlay"></div>
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-12 col-md-offset-0 text-left">
					

					<div class="row row-mt-15em">
						<div class="col-md-7 mt-text animate-box" data-animate-effect="fadeInUp">
							<h1>Welcome To Rettwit. Home of the most famous twit</h1>	
						</div>
						<div class="col-md-4 col-md-push-1 animate-box" data-animate-effect="fadeInRight">
							<div class="form-wrap">
								<div class="tab">
									
									<div class="tab-content">
										<div class="tab-content-inner active" data-content="signup">
                                        <div class="users form">
                                                <?php echo $this->Flash->render('auth'); ?>
                                                <?php echo $this->Form->create('User'); ?>
                                                    <fieldset>
                                                        <legend>
                                                            <?php echo __('Please enter your username and password'); ?>
                                                        </legend>
                                                        <?php echo $this->Form->input('username', array('class' => 'form-control'));
														echo $this->Form->input('password', array('class' => 'form-control',));
														echo $this->Form->submit('Login', array('class' => 'btn btn-primary btn-block'));
														echo $this->Html->link(
															'Register',
															'add'
														);
                                                        ?>
                                                    </fieldset>	
												<?php echo $this->Form->end();  
													 ?>
                                                </div>
											
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
							
					
				</div>
			</div>
		</div>
	</header>