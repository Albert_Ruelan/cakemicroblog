
<header id="gtco-header" class="gtco-cover gtco-cover-sm" role="banner" style="background-image: url(images/img_bg_3.jpg)">
		<div class="overlay"></div>
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-12 col-md-offset-0 text-left">
					<div class="row row-mt-15em">

						<div class="col-md-7 mt-text animate-box" data-animate-effect="fadeInUp">
							<span class="intro-text-small">Don't be shy</span>
							<h1>Get In Touch</h1>	
						</div>
						
					</div>
					
				</div>
			</div>
		</div>
	</header>
	<div class="gtco-section border-bottom">
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-6 animate-box">
					<h3>Get In Touch</h3>
<div class="users form">
<?php echo $this->Form->create('User', array('type' => 'file')); ?>
	<fieldset>
		<legend><?php echo __('Edit User'); ?></legend>
	<?php
echo $this->Form->input('picture', array('type' => 'file', 'class' => 'btn btn-primary btn-block'));
echo $this->Form->input('email', array('class' => 'form-control'));
echo $this->Form->input('password', array('class' => 'form-control'));
echo $this->Form->input('firstname', array('class' => 'form-control'));
echo $this->Form->input('lastname', array('class' => 'form-control'));
echo $this->Form->input('username', array('class' => 'form-control'));
echo $this->Form->submit('submit', array('class' => 'btn btn-primary btn-block'));

?>
	</fieldset>
<?php echo $this->Form->end(); ?>
</div>
</div>
				<div class="col-md-5 col-md-push-1 animate-box">
					
					<div class="gtco-contact-info">
						<h3>Contact Information</h3>
						<div class="actions">
							<h3><?php echo __('Actions'); ?></h3>
							<ul>

								<li><?php echo $this->Form->postLink(__('Delete This Account'), array('action' => 'delete', $this->Form->value('User.id')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('User.id')))); ?></li>
								<li><?php //echo $this->Html->link(__('List Users'), array('action' => 'index')); ?></li>
							</ul>
						</div>
					</div>


				</div>
				</div>
			</div>
		</div>
	</div>

</div>
</div>

</div>

