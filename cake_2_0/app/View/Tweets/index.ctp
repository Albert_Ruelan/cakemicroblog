<header id="gtco-header" class="gtco-cover gtco-cover-sm" role="banner" style="background-image: img/img_bg_3.jpg">
<div class="overlay"></div>
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-12 col-md-offset-0 text-left">
					<div class="row row-mt-15em">

						<div class="col-md-7 mt-text animate-box" data-animate-effect="fadeInUp">
							<span class="intro-text-small"><?php echo "See the posts from people  " ;?></span>
							<h1><?php echo "Home Page";  ?></h1>	
						</div>
						
					</div>
					
				</div>
			</div>
		</div>
	</header>
<div class="gtco-section" style="background-image: img/img_bg_3.jpg" >


			<div class="row">
				<div class="col-md-4 col-sm-6">
					<div class="feature-center animate-box" data-animate-effect="fadeIn">
						<span class="icon">
							<i>1</i>
						</span>
						<?php
							$path = 'img/'.$user["User"]["picture"].'';
							echo '<img src="/cake_2_0/app/webroot/img/'.$user["User"]["picture"].'" height="200"  ><br>'; 
							echo 'Hello : '.$user["User"]["firstname"] ." ".$user["User"]["lastname"];
							echo '<br> Username :'.$user["User"]["username"];
							echo '<br> Date Joined: :'.$user["User"]["created"];
						?>

						<h3>Total Number of followers : <?php echo $this->Html->link(__($countfollower), array('action' => 'viewfollower', $user["User"]["id"]));  ?></h3>

						<h3>Total Number of followings: <?php echo $this->Html->link(__($countfollowing), array('action' => 'viewfollowing', $user["User"]["id"])); ?></h3>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="feature-center animate-box" data-animate-effect="fadeIn">
						<span class="icon">
							<i>2</i>
						</span>
						<div class="actions">
							<?php echo $this->Form->create('Tweet'); ?>
							<fieldset>
								<legend><?php echo __('Add Tweet'); ?></legend>
							<?php
								echo $this->Form->input('content',array('class' => 'form-control' ,'rows' => 2));
								echo '<hr>';
								echo $this->Form->submit('Submit Tweet', array('class' => 'btn btn-primary btn-block'));

							?>
							</fieldset>
							<?php echo $this->Form->end(); ?>
						</div>
						<table cellpadding="0" cellspacing="0">
							<thead>
							<tr>
									<th><?php echo $this->Paginator->sort('tweetsid'); ?></th>
									<th><?php echo $this->Paginator->sort('content'); ?></th>
									<th class="actions"><?php echo __('Actions'); ?></th>
							</tr>	
							</thead>
							<tbody>
							<?php foreach ($tweets as $tweet):
								$i=0; ?>
							<tr>

								<td><?php 
								echo '<img src="/cake_2_0/app/webroot/img/'.$tweet[0]["picture"].'" height="200"  style="height:100px;width:100px;float:left;border-radius: 8px;" ><br>'; ?>&nbsp;</td>
								<td><?php echo h($tweet[0]['firstname']." ".$tweet[0]['lastname']).('<br>'.$tweet[0]['content']) .'<br>' .($tweet[0]['tweetcreated']); ?>&nbsp;</td>
								<td class="actions">
									<?php if($tweet[0]["userid"] == $_SESSION["Auth"]["User"]["id"]): ?>
									<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $tweet[0]['tweetsid']), array('confirm' => __('Are you sure you want to delete # %s?', $tweet[0]['tweetsid']))); ?>
									<?php echo '<br>'.$this->Html->link(__('Edit'), array('action' => 'edit', $tweet[0]['tweetsid'])); ?><br>
									<?php endif;?>

									<?php echo $this->Html->link(__('Comment'), array('action' => 'addcomment', $tweet[0]['tweetsid'])); ?>
									<?php $flag = false;?>
									<?php if(!empty($hearts)):?>
									<?php foreach($hearts as $heart){
										if($tweet[0]['tweetsid'] == $heart["Heart"]["tweetsid"]&& $flag == false){
											echo $this->Form->postLink(__('Unheart'), array('controller'=> 'Comments','action' => 'unheart', $heart["Heart"]["heartsid"]));
											$flag = true;	
										}
									}
									?>
									<?php endif;?>

									<?php if($flag == false):?>
									<?php echo $this->Html->link(__('Heart'), array('action' => 'heart', $tweet[0]['tweetsid'])); ?>
									<?php endif;?>
									
									<?php $i++?>
								</td>
							</tr>


						<?php endforeach; ?>
							</tbody>
							</table>
							<hr>
								<div class="paging">
									<?php
										echo $this->Paginator->prev('< ' . __('previous '), array(), null, array('class' => 'prev disabled'));
										echo $this->Paginator->numbers(array('separator' => ' '));	
										echo $this->Paginator->next(__(' next') . ' >', array(), null, array('class' => 'next disabled'));
										?>
								</div>
								<p>
								<?php
								echo $this->Paginator->counter(array(
									'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
								));
								?>	</p>
						</div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="feature-center animate-box" data-animate-effect="fadeIn">
						<span class="icon">
							<i>3</i>
						</span>
							<p> The top 5 most trending hashtags</p>
							<?php for($i = 1;$i < 6;$i++):?>
							<?php 
									if($i-1<count($hashtags))
									 echo "Rank".$i." :".$this->Html->link(__($hashtags[($i -1)]["hashtags"]["NumberOfProducts"] ), array('action' => 'viewhashtags', $hashtags[($i -1)]["hashtags"]["NumberOfProducts"] )). " - " . $hashtags[($i -1)][0]["COUNT(content)"] . " Tweets <br>"; 

								//	echo .; 
									else
									echo "Rank".$i." : Nothing Follows  - 0 Tweets <br>"; 
							?>
							<?php endfor; ?>
							<?php foreach ($suggesteds as $suggested):
							$i=0;$j=0;$k=0; 
								$flag=false;?>
							<tr>
							<?php if($followers === 0)
								$followers=[];
								?>

							<?php while($i<count($suggested)): $j=0;?>
										<!--Checks If there are followers for the users-->									
								<?php $k=0;$l=0;?>
										<!--Checks if there are multiple followings from the person being followed by this user-->
									<?php while($k<count($followers)): ?>
										<!--The thirdloops checks whether the person is already followed. the flag would be true if the user is already being followed-->
											<?php if($suggested[$i]["Following"]["userid"] == $followers[$k]["Following"]["userid"]):?> 
											<?php $flag = true;?>
											<?php endif;  ?>
											<?php $k++;?>
									<?php endwhile;  $k=0?>
									<?php while($k<count($following)): ?>
										<!--The thirdloops checks whether the person is already followed. the flag would be true if the user is already being followed-->
											<?php if($suggested[$i]["Following"]["userid"] == $following[$k]["Following"]["userid"]):?> 
											<?php $flag = true;?>
											<?php endif;  ?>
											<?php $k++;?>
									<?php endwhile;  ?>
											<?php if($flag == false):?>
											
												<?php echo '<img src="/cake_2_0/app/webroot/img/'.$suggested[$i]["UserJoin"]["picture"].'" style="margin-left:80px;height:100px;width:100px;float:left;"  ><br>'; ?>
												<td><?php $id=$suggested[$i]["Following"]["userid"]  ?>&nbsp;</td>
												<td><?php echo $firstname=$suggested[$i]["UserJoin"]["firstname"]  ?>&nbsp;</td>
												<td><?php echo $lastname=$suggested[$i]["UserJoin"]["lastname"]  ?>&nbsp;</td>
												<?php echo $this->Html->link(__('Follow'), array('action' => 'followme', $suggested[$i]["UserJoin"]["id"])); ?><br><br><br>
												<?php array_push($followers,array("Following"=>array("userid" => $id))); 
												$k++; ?>
												</tr>
											<?php endif; $flag = false; ?>

								<?php $i++;endwhile; ?>

						<?php endforeach; ?>
						</div>
				</div>
				

	</div>
							</div>