<div class="tweets view">
<h2><?php echo __('Tweet'); ?></h2>
	<dl>
		<dt><?php echo __('Tweetsid'); ?></dt>
		<dd>
			<?php echo h($tweet['Tweet']['tweetsid']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Userid'); ?></dt>
		<dd>
			<?php echo h($tweet['Tweet']['userid']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Content'); ?></dt>
		<dd>
			<?php echo h($tweet['Tweet']['content']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Tweetcreated'); ?></dt>
		<dd>
			<?php echo h($tweet['Tweet']['tweetcreated']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Tweet'), array('action' => 'edit', $tweet['Tweet']['tweetsid'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Tweet'), array('action' => 'delete', $tweet['Tweet']['tweetsid']), array('confirm' => __('Are you sure you want to delete # %s?', $tweet['Tweet']['tweetsid']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Tweets'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tweet'), array('action' => 'add')); ?> </li>
	</ul>
</div>
