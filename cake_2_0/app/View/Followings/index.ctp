<header id="gtco-header" class="gtco-cover gtco-cover-sm" role="banner" style="background-image: url(images/img_bg_3.jpg)">
		<div class="overlay"></div>
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-12 col-md-offset-0 text-left">
					<div class="row row-mt-15em">

						<div class="col-md-7 mt-text animate-box" data-animate-effect="fadeInUp">
							<span class="intro-text-small">Your List of</span>
							<h1>Followings</h1>	
						</div>
						
					</div>
					
				</div>
			</div>
		</div>
	</header>
	
	
	<div class="gtco-section border-bottom">
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-6 animate-box">
					<h3>Followings</h3>
					<div class="followings index">
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('username'); ?></th>
			<th><?php echo $this->Paginator->sort('username'); ?></th>
			<th><?php echo $this->Paginator->sort('datefollowed'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>	
	<tbody>
	<?php foreach ($followings as $following) : ?>
	<tr>

		<td><?php echo '<img src="/cake_2_0/app/webroot/img/' . $following["UserJoin"]["picture"] . '" height="200"  style="height:100px;width:100px;float:left;border-radius: 8px;" ><br>'; ?>&nbsp;</td>&nbsp;</td>
		<td><?php echo h($following["UserJoin"]["username"]); ?>&nbsp;</td>
		<td><?php echo h($following["Following"]['datefollowed']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $following["UserJoin"]["id"])); ?>
			<?php //echo $this->Html->link(__('Edit'), array('action' => 'edit', $following['followersid'])); ?>
			<!-- <?php echo $this->Form->postLink(__('Unfollow'), array('action' => 'delete', $following["Following"]["followersid"]), array('confirm' => __('Are you sure you want to Unfollow # %s?', $following['followid']))); ?> -->
		</td>
	</tr>
<?php endforeach; ?>

	</div>
	</tbody>
	</table>
	<center>
	<div class="paging">
	<?php
	echo $this->Paginator->prev('< ' . __('previous '), array(), null, array('class' => 'prev disabled'));
	echo $this->Paginator->numbers(array('separator' => ' '));
	echo $this->Paginator->next(__('next ') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
	</center>
	<p>
	<?php
		echo $this->Paginator->counter(array(
			'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
		));
	?>	</p>

</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Following'), array('action' => 'add')); ?></li>
	</ul>
</div>



				</div>
				<div class="col-md-5 col-md-push-1 animate-box">
					
					<div class="gtco-contact-info">
						<h3>Your Most Active Followings based on their number Twits: (By Rank)</h3>
						<ul>
							<?php $count = 1;
						foreach ($actives as $active) : ?>
							<li class="email"><?php echo "Rank: " . $count . "." . " " . $active["Userid"] . " With " . $active["NumberofTweets"] . " Tweets " ?></li>
							<?php $count++;
						endforeach; ?>
						</ul>
					</div>


				</div>
				</div>
			</div>
		</div>
	</div>