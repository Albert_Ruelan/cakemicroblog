<div class="followings form">
<?php echo $this->Form->create('Following'); ?>
	<fieldset>
		<legend><?php echo __('Edit Following'); ?></legend>
	<?php
		echo $this->Form->input('FollowID');
		echo $this->Form->input('UserID');
		echo $this->Form->input('FollowersID');
		echo $this->Form->input('DateFollowed');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Following.FollowID')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('Following.FollowID')))); ?></li>
		<li><?php echo $this->Html->link(__('List Followings'), array('action' => 'index')); ?></li>
	</ul>
</div>
