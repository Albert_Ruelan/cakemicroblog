<div class="followings view">
<h2><?php echo __('Following'); ?></h2>
	<dl>
		<dt><?php echo __('FollowID'); ?></dt>
		<dd>
			<?php echo h($following['Following']['FollowID']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('UserID'); ?></dt>
		<dd>
			<?php echo h($following['Following']['UserID']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('FollowersID'); ?></dt>
		<dd>
			<?php echo h($following['Following']['FollowersID']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('DateFollowed'); ?></dt>
		<dd>
			<?php echo h($following['Following']['DateFollowed']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Following'), array('action' => 'edit', $following['Following']['FollowID'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Following'), array('action' => 'delete', $following['Following']['FollowID']), array('confirm' => __('Are you sure you want to delete # %s?', $following['Following']['FollowID']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Followings'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Following'), array('action' => 'add')); ?> </li>
	</ul>
</div>
