<header id="gtco-header" class="gtco-cover gtco-cover-sm" role="banner" style="background-image: url(images/img_bg_3.jpg)">
		<div class="overlay"></div>
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-12 col-md-offset-0 text-left">
					<div class="row row-mt-15em">

						<div class="col-md-7 mt-text animate-box" data-animate-effect="fadeInUp">
							<span class="intro-text-small"><?php echo $confirmation; ?></span>
							<h1><?php echo $result; echo " "; echo $stringredirect;?></h1>	
							<div class="col-md-3 col-sm-6 animate-box" data-animate-effect="fadeInUp">
							<div id="gtco-counter" class="">
								<div class="feature-center">
								<span class="counter js-counter" data-from="5" data-to="0" data-speed="5000" data-refresh-interval="50">5</span>
								<span class="intro-text-small">Seconds</span>
								<script type = text/javascript>	    window.setTimeout(function(){
										// Move to a new location or you can do something else
										window.location.href = "http://localhost/cake_2_0/users/login";
										}, 5000);
									</script>
								</div>
							</div>
							<?php  ?>
						</div>
						</div>
						
					</div>
					
				</div>
			</div>
		</div>
	</header>
	