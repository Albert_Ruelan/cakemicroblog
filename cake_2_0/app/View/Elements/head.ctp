<?php
    echo $this->Html->charset();

    echo $this->Html->css('animate.css');
    echo $this->Html->css('bootstrap-datepicker.min');
    echo $this->Html->css('bootstrap.css');
    echo $this->Html->css('flexslider');    
    echo $this->Html->css('icomoon');
    echo $this->Html->css('magnific-popup');
    echo $this->Html->css('owl.carousel.min');
    echo $this->Html->css('owl.theme.default.min');
    echo $this->Html->css('style.css');
    echo $this->Html->css('themify-icons');
    echo $this->Html->script('jquery.min.js');
    echo $this->Html->script('jquery.stellar.min.js');
    echo $this->Html->script('jquery.waypoints.min');
    echo $this->Html->script('bootstrap.min.js');
    echo $this->Html->script('bootstrap-datepicker.min.js');
    echo $this->Html->script('jquery.countTo.js');
    echo $this->Html->script('jquery.easing.1.3.js');
    echo $this->Html->script('main.js');
    echo $this->Html->script('modernizr-2.6.2.min');
    echo $this->Html->script('owl.carousel.min');
    echo $this->Html->script('respond.min');
    echo $this->Html->script('jquery.easypiechart.min');
    echo $this->Html->script('jquery.magnific-popup.min');
    echo $this->Html->script('magnific-popup-options');
    echo $this->Html->script('respond.min');
    echo $this->Html->script('microblogajax');








?>
<title>CakePHP Tutorial by CodexWorld</title>