<div class="hashtags form">
<?php echo $this->Form->create('Hashtag'); ?>
	<fieldset>
		<legend><?php echo __('Add Hashtag'); ?></legend>
	<?php
		echo $this->Form->input('content');
		echo $this->Form->input('count');
		echo $this->Form->input('datecreated');
		echo $this->Form->input('hashtagtweetid');
		echo $this->Form->input('hashtaguserid');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Hashtags'), array('action' => 'index')); ?></li>
	</ul>
</div>
