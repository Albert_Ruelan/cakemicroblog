<header id="gtco-header" class="gtco-cover gtco-cover-sm" role="banner" style="background-image: img/img_bg_3.jpg">
		<div class="overlay"></div>
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-12 col-md-offset-0 text-left">
					<div class="row row-mt-15em">

						<div class="col-md-7 mt-text animate-box" data-animate-effect="fadeInUp">
							<span class="intro-text-small"><?php echo $tweetdetails["Tweet"]["tweetcreated"]; ?></span>
							<h1><?php echo $tweetdetails["Tweet"]["content"]; ?></h1>	
						</div>
						
					</div>
					
				</div>
			</div>
		</div>
	</header>

	<div class="gtco-section border-bottom">
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-6 animate-box">
					<h3>Comments</h3>
					<div class="followings index">
								<?php if($comments!=0):?>
								<?php foreach($comments as $comment): ?>
								<?php echo '<img src="/cake_2_0/app/webroot/img/'.$comment["UserJoin"]["picture"].'" height="200"  style="height:100px;width:100px;float:left;border-radius: 8px;" >'; ?>
								<p style=><?php echo $comment["UserJoin"]["firstname"]. " " ;echo $comment["UserJoin"]["lastname"]. '<br>'. $comment["UserJoin"]["email"].'<br>'.$comment["Comment"]["content"].'<br>';?>
								<?php if($comment["UserJoin"]["id"] == $_SESSION["Auth"]["User"]["id"]):?>
									<?php echo "  " .$this->Html->link(__('Edit'), array('action' => 'edit', $comment["Comment"]["commentid"])) ."   ".$this->Html->link(__('Delete'), array('action' => 'delete', $comment["Comment"]["commentid"])); ; ?>
								<?php endif; ?>
							</p>	
								
								<?php endforeach; ?>
								<?php endif;?>
	</tbody>
    </table>
    <center>
    <div class="paging">

    </div>
    </center>

</div>
<div class="actions">
	<?php echo $this->Form->create('Comment'); ?>
	<fieldset>
		<legend><?php echo __('Add Comment'); ?></legend>
	<?php
		echo $this->Form->input('content',array('class' => 'form-control' ,'rows' => 2));
		echo '<hr>';
		echo $this->Form->submit('SubmitComment', array('class' => 'btn btn-primary btn-block',$tweetdetails["Tweet"]["tweetsid"]));

	?>
	</fieldset>
	<?php echo $this->Form->end(); ?>
</div>



				</div>
				<div class="col-md-5 col-md-push-1 animate-box">
					
					<div class="gtco-contact-info">
						<?php if(empty($heart)):?>
						<?php echo $this->Html->link( $this->Html->image('heart-icon.png')."Heart",'http://localhost/cake_2_0/Comments/heart/'.$tweetdetails["Tweet"]["tweetsid"],array('escape' => false,$tweetdetails["Tweet"]["tweetsid"])); ?>
						<?php endif;?>
						<?php if(!empty($heart)):?>
						
						<?php echo $this->Form->postLink( $this->Html->image('unheart-icon.png')."Unheart",'http://localhost/cake_2_0/Comments/unheart/'.$heart["Heart"]["heartsid"],array('escape' => false,$tweetdetails["Tweet"]["tweetsid"])); ?>
						<?php endif;?>
						<?php echo $this->Form->postLink( $this->Html->image('heart-icon.png')."Retweet",'http://localhost/cake_2_0/Comments/retweet/'.$tweetdetails["Tweet"]["tweetsid"],array('escape' => false,$tweetdetails["Tweet"]["tweetsid"])); ?>
						<div class="actions">
						<?php echo $this->Form->create('Retweet'); ?>
							<fieldset>
								<legend><?php echo __('Add Text to retweet'); ?></legend>
							<?php
								echo $this->Form->input('content',array('class' => 'form-control','rows' => 2));
								echo '<hr>';
								echo $this->Form->submit('SubmitRetweet', array('action' => 'retweet','class' => 'btn btn-primary btn-block',$tweetdetails["Tweet"]["tweetsid"]),'http://localhost/cake_2_0/Comments/retweet/');
							?>
							</fieldset>
							<?php echo $this->Form->end(); ?>
						</div>
						
						
						<hr>
						<h3>People who hearted this tweets</h3>
						<ul>
						<?php if(!empty($numpeoplehearts)):?>
							<?php $count =1; foreach($numpeoplehearts as $numpeopleheart): ?>
							<?php echo '<img src="/cake_2_0/app/webroot/img/'.$numpeopleheart["UserJoin"]["picture"].'" height="200"  style="height:100px;width:100px;float:left;border-radius: 8px;" >'; ?>
							<p style=><?php echo $numpeopleheart["UserJoin"]["firstname"]. " " ;echo $numpeopleheart["UserJoin"]["lastname"]. '<br>'." Date Hearted : " .$numpeopleheart["Heart"]["datehearted"].'<br>';?>
							<br><br>
							<?php $count++; endforeach;?>
						<?php endif;?>
						</ul>
					</div>


				</div>
				</div>
			</div>
		</div>
	</div>