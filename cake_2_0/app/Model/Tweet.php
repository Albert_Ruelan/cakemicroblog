<?php
App::uses('AppModel', 'Model');
/**
 * Tweet Model
 *
 */
class Tweet extends AppModel {

	public function profiletweets($currentId){
		$sqlTweet = $this->findByuserid($currentId);
		return $sqlTweet;
        }
	public function paginate($conditions, $fields, $order, $limit, $page = 1, $recursive = null, $extra = array()) {    
        $recursive = -1;
        // Mandatory to have
        $this->useTable = false;
        $sql = '';        
        $id=$this->id;
        $sql .= "SELECT tweet.tweetsid,tweet.userid,tweet.content, userss.lastname,userss.firstname,tweet.tweetcreated,userss.picture from `tweets` as tweet Right JOIN `followings` as follow on follow.userid = tweet.userid LEFT Join `users` as userss on follow.userid = userss.id WHERE follow.followersid = $id Union ALL SELECT c1.tweetsid,c1.userid , c1.content,c2.lastname,c2.firstname,c1.tweetcreated,c2.picture From `tweets` as c1 Right JOIN `users` as c2 on c2.id = c1.userid WHERE c1.userid = $id Order By tweetcreated DESC ";
        $limit = 5;
        // Adding LIMIT Clause
        $sql .= "LIMIT ".(($page - 1) * $limit) . ', ' . $limit;
        $results = $this->query($sql);
        return $results;
        }
        

        ////////////////END
        public function usertweetslist($id){
                return $this->find('all',array('conditions'=>array('Tweet.userid'=>$id)));
	}

	public function paginateCount($conditions = null, $recursive = 0, $extra = array()) {
        $sql = '';
        $id=$_SESSION["Auth"]["User"]["id"];
        $sql .= "SELECT tweet.tweetsid,tweet.userid,tweet.content, userss.lastname,userss.firstname,tweet.tweetcreated,userss.picture from `tweets` as tweet Right JOIN `followings` as follow on follow.userid = tweet.userid LEFT Join `users` as userss on follow.userid = userss.id WHERE follow.followersid = $id Union ALL SELECT c1.tweetsid,c1.userid , c1.content,c2.lastname,c2.firstname,c1.tweetcreated,c2.picture From `tweets` as c1 Right JOIN `users` as c2 on c2.id = c1.userid WHERE c1.userid = $id Order By tweetcreated DESC ";
        $this->recursive = $recursive;
        $results = $this->query($sql);
        return count($results);
    }
/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'tweetsid';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'tweetsid';

}
