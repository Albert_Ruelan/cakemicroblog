<?php
App::uses('AppModel', 'Model');
/**
 * Following Model
 *
 */
class Following extends AppModel {
	 /// user created function
	 public function getcountfollowersfollowing($sql){
		 if($sql != null){
		$number = count($sql); 
			if($sql == 0 ){
				 return 0;
			 }
			 else{
			 return $number;
			 }}
			 else
			 return 0;
	 }
	 public function getfollower($currentId)
	 {
		$sqlFollower = $this->find('all', array(
			'joins' => array(
				array(
					'table' => 'users',
					'alias' => 'UserJoin',
					'type' => 'INNER',
					'conditions' => array(
						'UserJoin.id = Following.followersid'

					)
				)
			),
			'conditions' => array(
				'Following.userid' => $currentId
			),
			'fields' => array('UserJoin.*', 'Following.*'),
			'order' => 'Following.datefollowed DESC'
		));
		if (empty($sqlFollower)) {
		   return $sqlFollower = 0;
	   }
	   else
		return $sqlFollower;	
	 }
	 public function bubblesort($arr){
		 for ($i=0; $i <count($arr) ; $i++) {
			 for ($j=0; $j <count($arr)-$i -1; $j++) { 
				if ($arr[$j]["NumberofTweets"]< $arr[$j+1]["NumberofTweets"]) {
					$temp = $arr[$j];
					$arr[$j] = $arr[$j + 1];
					$arr[$j + 1] = $temp;
				  }
			 } 
		 }
		 return $arr;
	 }

	 public function getfollowing($currentId)
	 {
 
		 $sqlFollower = $this->find('all', array(
			 'joins' => array(
				 array(
					 'table' => 'users',
					 'alias' => 'UserJoin',
					 'type' => 'INNER',
					 'conditions' => array(
						 //UserJoin.id = Following.followersid'
						'UserJoin.id = Following.userid'
					 )
				 )
			 ),
			 'conditions' => array(
				 'Following.followersid' => $currentId
			 ),
			 'fields' => array('UserJoin.*', 'Following.*'),
			 'order' => 'Following.datefollowed DESC'
		 ));
		 if (empty($sqlFollower)) {
			return $sqlFollower = 0;
		}
		else
		 return $sqlFollower;
	 }
	 public function getsuggest($sqlFollower)
	 {
		 $i = 0;
		if($sqlFollower != null){
		 $suggestedpeople = [];
		 while ($i < count($sqlFollower)) {
			 $sqlSuggestedFollower = $this->find('all', array(
				 'joins' => array(
					 array(
						 'table' => 'users',
						 'alias' => 'UserJoin',
						 'type' => 'INNER',
						 'conditions' => array(
							 'UserJoin.id = Following.userid'
						 )
					 )
				 ),
				 'conditions' => array(
					 'Following.followersid' => $sqlFollower[$i]["Following"]["userid"]
				 ),
				 'fields' => array('UserJoin.*', 'Following.*'),
				 'order' => 'Following.datefollowed DESC'
			 ));
			 $i++;
			 array_push($suggestedpeople, $sqlSuggestedFollower);
		 }
		 return $suggestedpeople;
		}else{
		 return  $suggestedpeople = [];
		}
	 }
 


/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'followid';

/**
 * Validation rules
 *
 * @var array
 */
	
}
