<?php
App::uses('AppModel', 'Model');
/**
 * Comment Model
 *
 */
class Comment extends AppModel {

/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'commentid';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'commentid';
	public function getcomments($currentId)
	{
	   $sqlFollower = $this->find('all', array(
		   'joins' => array(
			   array(
				   'table' => 'users',
				   'alias' => 'UserJoin',
				   'type' => 'INNER',
				   'conditions' => array(
					   'UserJoin.id = Comment.userid'
				   )
				   ),

		   ),
		   'conditions' => array(
			   'Comment.tweetid' => $currentId
		   ),
		   'fields' => array('UserJoin.*','Comment.*'),
	   ));
	   if (empty($sqlFollower)) {
		  return $sqlFollower = 0;
	  }
	  else
	   return $sqlFollower;	
	}

}
