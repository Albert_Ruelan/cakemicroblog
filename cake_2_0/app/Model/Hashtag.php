<?php
App::uses('AppModel', 'Model');
/**
 * Hashtag Model
 *
 */
class Hashtag extends AppModel {

/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'hashtagid';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'hashtagid';
	public function hashtags($weetsid,$text,$time){
		$results = explode(" ", $text);
		for($i=0;$i<count($results);$i++){
			$this->create();
			if( preg_match('/(?:^|\s)#(\w+)/', $results[$i]) ==true){
				$checker=$this->checkifnotexist($results[$i]);
				if($checker === true){
				$text = strtoupper($results[$i]);
				$this->data['Hashtag']['count'] = 1;
				$this->data['Hashtag']['datecreated'] = $time;
				$this->data['Hashtag']['hashtaguserid'] =	$_SESSION["Auth"]["User"]["id"];
				$this->data['Hashtag']['hashtagtweetid'] = $weetsid;
				$this->data['Hashtag']['content'] =  $text;
				$this->save($this->data);}
				else{				
					$text = strtoupper($results[$i]);
					$this->data['Hashtag']['count'] = $checker["Hashtag"]["count"] +1;
					$this->data['Hashtag']['datecreated'] = $time;
					$this->data['Hashtag']['hashtaguserid'] =	$_SESSION["Auth"]["User"]["id"];
					$this->data['Hashtag']['hashtagtweetid'] = $weetsid;
					$this->data['Hashtag']['content'] =  $text;
					$this->save($this->data);
				}
			}
		}
	}
	public function getfollowing($currentId)
	{

		$sqlFollower = $this->find('all', array(
			'joins' => array(
				array(
					'table' => 'users',
					'alias' => 'UserJoin',
					'type' => 'INNER',
					'conditions' => array(
						//UserJoin.id = Following.followersid'
					   'UserJoin.id = Following.userid'
					)
				)
			),
			'conditions' => array(
				'Following.followersid' => $currentId
			),
			'fields' => array('UserJoin.*', 'Following.*'),
			'order' => 'Following.datefollowed DESC'
		));
		if (empty($sqlFollower)) {
		   return $sqlFollower = 0;
	   }
	   else
		return $sqlFollower;
	}
	public function gethash($content)
	{
		$i = 0;
	   if($content != null){
		$hashtags = [];
			$hashtags = $this->find('all', array(
				'joins' => array(
					array(
						'table' => 'users',
						'alias' => 'UserJoin',
						'type' => 'INNER',
						'conditions' => array(
							'UserJoin.id = Hashtag.hashtaguserid'
						)
					),
					array(
						'table' => 'tweets',
						'alias' => 'TweetJoin',
						'type' => 'INNER',
						'conditions' => array(
							'TweetJoin.tweetsid = Hashtag.hashtagtweetid'
						)
					)
				),
				'conditions' => array(
					'Hashtag.content' => $content
				),
				'fields' => array('UserJoin.*', 'TweetJoin.*','Hashtag.*'),
				'order' => 'Hashtag.datecreated DESC'
			));
		
		return $hashtags;
	   }else{
		return  $hashtags = [];
	   }
	}
	public function gethashtaglist(){
		$hashtags=$this->query("SELECT COUNT(content),content AS NumberOfProducts FROM hashtags GROUP By content ORDER BY COUNT(content)DESC ");
		return $hashtags;
	}
	public function checkifnotexist($hashtag){
		$hash=$this->findBycontent($hashtag);
		if(empty($hash)){
			return true;
		}
		else 
		return $hash;
	}

}
