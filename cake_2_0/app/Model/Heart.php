<?php
App::uses('AppModel', 'Model');
/**
 * Heart Model
 *
 */
class Heart extends AppModel {

/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'heartsid';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'heartsid';
	public function getpeopleheart($currentId){
		$sqlHeart = $this->find('all', array(
			'joins' => array(
				array(
					'table' => 'users',
					'alias' => 'UserJoin',
					'type' => 'INNER',
					'conditions' => array(
						'UserJoin.id = Heart.userid'
					)
					),
 
			),
			'conditions' => array(
				'Heart.tweetsid' => $currentId
			),
			'fields' => array('UserJoin.*','Heart.*'),
		));
		if (empty($sqlHeart)) {
		   return $sqlHeart = 0;
	   }
	   else
		return $sqlHeart;	
	 
	}
}
