<?php
App::uses('AppModel', 'Model');
/**
 * Tempholder Model
 *
 */
class Tempholder extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'tempholder';

/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'tempid';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'tempid';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'confirmcode' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);
	public function addconfirmcode($confirm_code,$currentid){
		date_default_timezone_set('Asia/Manila');
		$mysqltimeCurrent = date("Y-m-d H:i:s");
		$this->data['Tempholder']['confirmcode'] = $confirm_code;
		$this->data['Tempholder']['userid'] = $currentid;
		$this->data['Tempholder']['tempdate'] = $mysqltimeCurrent;

		$this->save($this->data);

	}
	public function findconfirmcode($confirmcode){
		$sqlUser = $this->findByconfirmcode($confirmcode);
		return $sqlUser;
	}
}
