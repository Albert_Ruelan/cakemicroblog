<?php
App::uses('AppModel', 'Model');
App::uses('BlowfishPasswordHasher', 'Controller/Component/Auth');
class User extends AppModel {
    
    public function getuser($currentId){
		$sqlUser = $this->findByid($currentId);
		return $sqlUser;
    }
    public function unique_multidim_array($array, $key) {
    if(!empty($array[0])==true){
    $temp_array = array();
    $i = 0;
    $key_array = array();
   
    foreach($array as $val) {
        if (!in_array($val["User"][$key], $key_array)) {
            $key_array[$i] = $val["User"][$key];
            $temp_array[$i] = $val;
        }
        $i++;
    }
    return $temp_array;
    }
    return $temp_array=[];
}

    
    public function beforeSave($options = array()) {
        if (isset($this->data[$this->alias]['password'])) {
            $passwordHasher = new BlowfishPasswordHasher();
            $this->data[$this->alias]['password'] = $passwordHasher->hash(
                $this->data[$this->alias]['password']
            );
        }
        return true;
    }public function sendConfirmation($confirm_code, $registerEmail)
    {
          require 'mailer/Exception.php';
          require 'mailer/PHPMailer.php';
          require 'mailer/SMTP.php';

          $mail = new PHPMailer(true);                              // Passing `true` enables exceptions
          try {
              //Server settings
                $mail->isSMTP();                                      // Set mailer to use SMTP
                $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
                $mail->SMTPAuth = true;                               // Enable SMTP authentication
                $mail->Username = 'albert.ruelan07@gmail.com';                 // SMTP username
                $mail->Password = 'albertrey';                           // SMTP password
                $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
                $mail->Port = 587;                                    // TCP port to connect to

                $mail->setFrom('albert.ruelan07@gmail.com', 'Mailer');
                $mail->addAddress($registerEmail, 'Albert Ruelan');     // Add a recipient
                $mail->addReplyTo('albert.ruelan07@gmail.com', 'Information');
                $mail->addCC('cc@example.com');
                $mail->addBCC('bcc@example.com');
          
              //Content
                $mail->isHTML(true);                                  // Set email format to HTML
                $mail->Subject = 'Confirmation Email';
                $mail->Body = "This is your confirmation link : ";
                $mail->Body .= "http://localhost/cake_2_0/tempholders/index/$confirm_code";

                $mail->send();
                return true;
          } catch (Exception $e) {
                echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
          }
    }

    public $validate = array(
        'username' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'A username is required'
            )
        ),
        'email' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'A username is required'
            )
        ),
        'firstname' => array(
            'required' => array(
                'rule' => 'notBlank', 
                'message' => 'Firstname is required'
                )
        ),
        'lastname' => array(
            'required' => array(
                'rule' => 'notBlank', 
                'message' => 'lastname is required'
                )
        ),
        'picture'=> array(
            'required' => array(
                'rule' => 'notBlank', 
                'message' => 'picture name is required'
                )
        ),
        'password' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'A password is required'
            )
        )
    );
}
?>